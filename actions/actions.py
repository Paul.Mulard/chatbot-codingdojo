from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
import requests

class ActionWeather(Action):

    def name(self) -> Text:
         return "action_weather"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
         
        location = tracker.get_slot('location')

        import requests

        url = "https://weatherapi-com.p.rapidapi.com/current.json"

        querystring = {"q":location}

        headers = {
            "X-RapidAPI-Key": "7f7a91ce91msh5872d05c0e01ea0p1bbf6ajsn3f187e13f9c0",
            "X-RapidAPI-Host": "weatherapi-com.p.rapidapi.com"
        }

        response = requests.get(url, headers=headers, params=querystring)

        current_weather = response.json()["current"]

        if current_weather["temp_c"] <= 10:
            dispatcher.utter_message(f"Grrrr, il fait froid à {location}. Il fait {current_weather['temp_c']}°C")
        elif current_weather["temp_c"] <= 25:
            dispatcher.utter_message(f"Il fait plutôt bon à {location}. Il fait {current_weather['temp_c']}°C")
        else:
            dispatcher.utter_message(f"Oulala il fait chaud à {location}. Il fait {current_weather['temp_c']}°C")

        return []
